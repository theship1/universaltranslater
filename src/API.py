from flask import Flask
from flask import request
import json

from Artemis_Station import Artemis_Station
from Shangris_Station import Shangris_Station
from Elyse_Terminal import Elyse_Terminal
from Core_Station import Core_Station

app = Flask(__name__)


@app.route('/Artemis Station/receive', methods=['POST'])
def ArtemisReceive():
    return {"kind": "success", "messages": Artemis_Station.receive()}


@app.route('/Artemis Station/send', methods=['POST'])
def ArtemisSend():
    data = request.get_data()
    data = json.loads(data)
    return {"kind": Artemis_Station.send(data["source"], data["data"])}


@app.route('/Core Station/receive', methods=['POST'])
def CoreReceive():
    return Core_Station.receive()


@app.route('/Core Station/send', methods=['POST'])
def CoreSend():
    data = request.get_data()
    data = json.loads(data)
    return Core_Station.send(data["source"], data["data"])


@app.route('/Shangris Station/receive', methods=['POST'])
def ShangrisReceive():
    return {"kind": "success", "messages": Shangris_Station.receive()}


@app.route('/Shangris Station/send', methods=['POST'])
def ShangrisSend():
    data = request.get_data()
    data = json.loads(data)
    Shangris_Station.send(data["source"], data["data"])
    return {"kind": "success"}


@app.route('/Elyse Terminal/receive', methods=['POST'])
def ElyseReceive():
    return {"kind": "success", "messages": Elyse_Terminal.receive()}


@app.route('/Elyse Terminal/send', methods=['POST'])
def ElyseSend():
    data = request.get_data()
    data = json.loads(data)
    Elyse_Terminal.send(data["source"], data["data"])
    return {"kind": "success"}


app.run(host="0.0.0.0", debug=True, port=2023)
