import xmlrpc.client


class Artemis_Station:

    def send(source, data):
        with xmlrpc.client.ServerProxy(
                "http://192.168.0.3:2024/RPC2") as proxy:
            return proxy.send(source, bytearray(data))

    def receive():
        with xmlrpc.client.ServerProxy(
                "http://192.168.0.3:2024/RPC2") as proxy:

            converteteData = []
            data = proxy.receive()
            for destination, data in data:
                converteteData.append({
                    "destination": destination,
                    "data": list(data.data)
                })
            return converteteData
