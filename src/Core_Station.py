import requests
import json
import base64


class Core_Station:

    def send(source, data):
        url = 'http://192.168.0.3:2027/send'
        body = {
            "source": source,
            "message": base64.b64encode(bytes(data)).decode("utf-8")
        }

        return requests.post(url, json=body).content

    def receive():
        url = 'http://192.168.0.3:2027/receive'
        data = json.loads(requests.post(url).content)
        messages = []
        i = 0
        while i < len(data["received_messages"]):
            message = base64.b64decode(
                bytes(data["received_messages"][i]["data"], "utf-8")).decode("utf-8")
            messages.append(
                json.dumps({
                    "target": data["received_messages"][i]["target"],
                    "data": message
                }))
            i = i + 1

        return {"kind": data["kind"], "messages": messages}


Core_Station.send("Schnitzeljagd", "gugus")