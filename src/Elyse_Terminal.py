from websockets.sync.client import connect
import json
import time


class Elyse_Terminal:

    def receive():
        try:
            with connect("ws://192.168.0.3:2026/api") as websocket:
                messages = []
                t_end = time.time() + 1
                while time.time() < t_end:
                    data = json.loads(websocket.recv(0.2))
                    messages.append({
                        "destination": data["destination"],
                        "data": data["msg"]
                    })
                return messages
        except:
            return []

    def send(source, data):
        with connect("ws://192.168.0.3:2025/ws") as websocket:
            websocket.send(json.dumps({"source": source, "msg": data}))
