from websockets.sync.client import connect
import json
import time


class Shangris_Station:

    def receive():
        try:
            with connect("ws://192.168.0.3:2025/ws") as websocket:
                messages = []
                t_end = time.time() + 1
                while time.time() < t_end:
                    messages.append(websocket.recv(0.2))
                return messages
        except:
            return []

    def send(source, data):
        with connect("ws://192.168.0.3:2025/ws") as websocket:
            websocket.send(json.dumps({"source": source, "data": data}))
